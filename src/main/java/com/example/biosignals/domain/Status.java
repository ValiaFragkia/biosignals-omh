package com.example.biosignals.domain;

/**
 * Created by amenychtas on 29/11/2015.
 */

public class Status {
    private String id;

    private String message;

    public Status() {
    }

    public Status(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
