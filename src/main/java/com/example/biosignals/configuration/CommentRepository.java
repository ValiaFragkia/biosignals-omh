package com.example.biosignals.configuration;

import com.example.biosignals.domain.Biosignal;
import com.example.biosignals.domain.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by amenychtas on 29/11/2015.
 */

@RepositoryRestResource(collectionResourceRel = "comments", path = "comments")
public interface CommentRepository extends MongoRepository<Comment, String> {
    List<Comment> findByUserId(@Param("userId") String type);
}
