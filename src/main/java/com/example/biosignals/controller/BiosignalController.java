package com.example.biosignals.controller;

import com.example.biosignals.configuration.BiosignalRepository;
import com.example.biosignals.domain.Biosignal;
import com.example.biosignals.domain.BpmMeasurement;
import com.example.biosignals.domain.Measurement;
import com.example.biosignals.domain.OximeterMeasurement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by amenychtas on 29/11/2015.
 */
@RestController
public class BiosignalController {

    @Autowired
    BiosignalRepository biosignalRepository;

    @RequestMapping(value = "/custombiosignals", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> custombiosignals(@RequestBody List<Biosignal> biosignals) {

        List<Biosignal> storedBiosignals = biosignalRepository.save(biosignals);

        return new ResponseEntity<>(storedBiosignals, HttpStatus.OK);
    }

    @RequestMapping(value = "/getCSV/{measurementId}.csv", method = RequestMethod.GET, produces = "text/csv")
    public ResponseEntity<?> getCSV(@PathVariable("measurementId") String measurementId) {
        List<Biosignal> biosignals = biosignalRepository.findByMeasurementId(measurementId);

        StringBuilder sb = new StringBuilder();
        int flag =0;
        int count=0;
        int diasValue = 0;
        int diasValue1 = 0;
        int diasValue2;
        int systValue = 0;
        int systValue1 = 0;
        int systValue2;
        long timeStamp =0;
        long timeRelative =0;

        sb.append("UserId");
        sb.append(',');
        sb.append("MeasurementId");
        sb.append(',');
        sb.append("Ppg");
        sb.append(',');
        sb.append("Time");
        sb.append(',');
        sb.append("Systolic Pressure (avg)");
        sb.append(',');
        sb.append("Diastolic Pressure (avg)");
        sb.append(',');
        sb.append("Real Time Heart Rate");
        sb.append(',');
        sb.append("Perfusion Index");
        sb.append('\n');

        for (Biosignal biosignal : biosignals) {

            if (Objects.equals(biosignal.getType(), "oximeter")) {

                sb.append(biosignal.getUserId());
                sb.append(',');
                sb.append(biosignal.getMeasurementId());

                //time stamp
                if (flag == 0){ //only for the first biosignal
                    timeStamp = biosignal.getDate().getTime(); //in msec

                    //avg systolic & diastolic pressure
                    Biosignal firstBios = biosignals.get(0);
                    BpmMeasurement bpmMeas1 = firstBios.getBpmMeasurement();
                    diasValue1 = bpmMeas1.getDiastolic().getValue();

                    systValue1 = bpmMeas1.getSystolic().getValue();

                    Biosignal lastBios = biosignals.get(biosignals.size()-1);
                    BpmMeasurement bpmMeas2 = lastBios.getBpmMeasurement();
                    diasValue2 = bpmMeas2.getDiastolic().getValue();
                    diasValue = (diasValue1+diasValue2)/2;

                    systValue2 = bpmMeas2.getSystolic().getValue();
                    systValue = (systValue1+systValue2)/2;

                    flag++;
                }else {  //compute relative time
                    Date d = biosignal.getDate();
                    long timeInMsec= d.getTime();
                    timeRelative = timeInMsec - timeStamp;
                }

                //ppg
                OximeterMeasurement oximMeas = biosignal.getOximeterMeasurement();
                List<Integer> ppgLocal = oximMeas.getPpg();

                if (ppgLocal.size() != 0) {  //if ppg list is not empty

                    //for 1st biosignal
                    sb.append(',');
                    sb.append(ppgLocal.get(0));
                    sb.append(',');
                    sb.append(timeRelative);

                    //systolic, diastolic, heart rate,pi
                    sb.append(',');
                    sb.append(systValue);
                    sb.append(',');
                    sb.append(diasValue);
                    sb.append(',');
                    sb.append(oximMeas.getHeartRate().getValue());
                    sb.append(',');
                    sb.append(oximMeas.getPi().getValue());

                    sb.append('\n');

                    //for the rest biosignals
                    for (int i = 1; i < ppgLocal.size(); i++) {
                        //ppg
                        sb.append(',');
                        sb.append(',');
                        sb.append(ppgLocal.get(i));
                        sb.append(',');
                        timeRelative += 20;
                        sb.append(timeRelative);

                        //systolic, diastolic, heart rate,pi
                        sb.append(',');
                        sb.append(systValue);
                        sb.append(',');
                        sb.append(diasValue);

                        sb.append(',');
                        sb.append(oximMeas.getHeartRate().getValue());
                        sb.append(',');
                        sb.append(oximMeas.getPi().getValue());

                        sb.append('\n');
                    }
                }
            }
        }
        return new ResponseEntity<>(sb.toString(), HttpStatus.OK);
    }
}