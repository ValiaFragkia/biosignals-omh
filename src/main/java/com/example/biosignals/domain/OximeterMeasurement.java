package com.example.biosignals.domain;

import java.util.List;

/**
 * Created by amenychtas on 29/11/2015.
 */
public class OximeterMeasurement {
    private Measurement oxygenSaturation;
    private Measurement heartRate;
    private Measurement pi;
    private List<Integer> ppg;

    public OximeterMeasurement() {
    }

    public OximeterMeasurement(Measurement oxygenSaturation, Measurement heartRate, Measurement pi, List<Integer> ppg) {
        this.oxygenSaturation = oxygenSaturation;
        this.heartRate = heartRate;
        this.ppg = ppg;
        this.pi = pi;
    }

    public Measurement getOxygenSaturation() {
        return oxygenSaturation;
    }

    public void setOxygenSaturation(Measurement oxygenSaturation) {
        this.oxygenSaturation = oxygenSaturation;
    }

    public Measurement getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(Measurement heartRate) {
        this.heartRate = heartRate;
    }

    public Measurement getPi() { return pi; }

    public List<Integer> getPpg() {
        return ppg;
    }

    public void setPi(Measurement pi) { this.pi = pi; }

    public void setPpg(List<Integer> ppg) {
        this.ppg = ppg;
    }
}