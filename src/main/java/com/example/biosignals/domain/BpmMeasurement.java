package com.example.biosignals.domain;

/**
 * Created by amenychtas on 29/11/2015.
 */
public class BpmMeasurement {
    private Measurement systolic;
    private Measurement diastolic;
    private Measurement heartRate;

    public BpmMeasurement() {
    }

    public BpmMeasurement(Measurement systolic, Measurement diastolic, Measurement heartRate) {
        this.systolic = systolic;
        this.diastolic = diastolic;
        this.heartRate = heartRate;
    }

    public Measurement getSystolic() {
        return systolic;
    }

    public void setSystolic(Measurement systolic) {
        this.systolic = systolic;
    }

    public Measurement getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(Measurement diastolic) {
        this.diastolic = diastolic;
    }

    public Measurement getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(Measurement heartRate) {
        this.heartRate = heartRate;
    }
}
