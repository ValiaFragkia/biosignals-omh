package com.example.biosignals.configuration;

import com.example.biosignals.domain.Biosignal;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by amenychtas on 29/11/2015.
 */

@RepositoryRestResource(collectionResourceRel = "biosignals", path = "biosignals")
public interface BiosignalRepository extends MongoRepository<Biosignal, String> {
    List<Biosignal> findByType(@Param("type") String type);

    List<Biosignal> findByMeasurementId(@Param("measurementId") String measurementId);

    List<Biosignal> findByUserId(@Param("userId") String userId);
}
