package com.example.biosignals.domain;

/**
 * Created by amenychtas on 29/11/2015.
 */
public class Measurement {
    private String unit;
    private int value;

    public Measurement() {
    }

    public Measurement(String unit, int value) {
        this.unit = unit;
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
