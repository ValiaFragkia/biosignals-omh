package com.example.biosignals.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Date;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 * Created by amenychtas on 29/11/2015.
 */
@JsonInclude(NON_NULL)
public class Biosignal {
    @Id
    private String id;

    @Indexed
    private String measurementId;

    @Indexed
    private String userId;

    private Date date;
    private String type;                                //If it is blood pressure or oximeter, e.g. values bpm and oximeter

    private OximeterMeasurement oximeterMeasurement;    //either this field will be filled in case of oximeter type
    private BpmMeasurement bpmMeasurement;              //or this one, in case of bpm

    public Biosignal() {
    }

    public Biosignal(String id, String measurementId, String userId, Date date, String type, OximeterMeasurement oximeterMeasurement, BpmMeasurement bpmMeasurement) {
        this.id = id;
        this.measurementId = measurementId;
        this.userId = userId;
        this.date = date;
        this.type = type;
        this.oximeterMeasurement = oximeterMeasurement;
        this.bpmMeasurement = bpmMeasurement;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMeasurementId() {
        return measurementId;
    }

    public void setMeasurementId(String measurementId) {
        this.measurementId = measurementId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public OximeterMeasurement getOximeterMeasurement() {
        return oximeterMeasurement;
    }

    public void setOximeterMeasurement(OximeterMeasurement oximeterMeasurement) {
        this.oximeterMeasurement = oximeterMeasurement;
    }

    public BpmMeasurement getBpmMeasurement() {
        return bpmMeasurement;
    }

    public void setBpmMeasurement(BpmMeasurement bpmMeasurement) {
        this.bpmMeasurement = bpmMeasurement;
    }
}
