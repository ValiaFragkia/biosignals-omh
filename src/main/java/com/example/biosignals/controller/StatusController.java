package com.example.biosignals.controller;

import com.example.biosignals.domain.Status;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by amenychtas on 29/11/2015.
 */
@RestController
public class StatusController {

    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> status() {
        Status s = new Status("OK");

        return new ResponseEntity<>(s, HttpStatus.OK);
    }
}
